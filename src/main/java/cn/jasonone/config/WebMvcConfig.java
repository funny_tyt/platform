package cn.jasonone.config;

import cn.jasonone.interceptors.AuthorizationInterceptor;
import cn.jasonone.interceptors.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	@Resource
	private LoginInterceptor loginInterceptor;
	@Resource
	private AuthorizationInterceptor authorizationInterceptor;
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptorRegistration = registry.addInterceptor(loginInterceptor);
		interceptorRegistration.addPathPatterns("/**");
		interceptorRegistration.excludePathPatterns("/login");
		interceptorRegistration.excludePathPatterns("/error");
		interceptorRegistration.excludePathPatterns("/lib/**");
		InterceptorRegistration authorization = registry.addInterceptor(authorizationInterceptor);
		authorization.addPathPatterns("/**");
	}
}
