package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.Topic;
import cn.jasonone.bean.TopicExample;
public interface TopicService{


    long countByExample(TopicExample example);

    int deleteByExample(TopicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Topic record);

    int insertSelective(Topic record);

    List<Topic> selectByExample(TopicExample example);

    Topic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Topic record,TopicExample example);

    int updateByExample(Topic record,TopicExample example);

    int updateByPrimaryKeySelective(Topic record);

    int updateByPrimaryKey(Topic record);

}
