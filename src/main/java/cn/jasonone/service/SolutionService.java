package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.SolutionExample;
import cn.jasonone.bean.Solution;
public interface SolutionService{


    long countByExample(SolutionExample example);

    int deleteByExample(SolutionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Solution record);

    int insertSelective(Solution record);

    List<Solution> selectByExample(SolutionExample example);

    Solution selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Solution record,SolutionExample example);

    int updateByExample(Solution record,SolutionExample example);

    int updateByPrimaryKeySelective(Solution record);

    int updateByPrimaryKey(Solution record);

}
