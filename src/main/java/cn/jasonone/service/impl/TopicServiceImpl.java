package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.bean.Topic;
import cn.jasonone.mapper.TopicMapper;
import cn.jasonone.bean.TopicExample;
import cn.jasonone.service.TopicService;
@Service
public class TopicServiceImpl implements TopicService{

    @Resource
    private TopicMapper topicMapper;

    @Override
    public long countByExample(TopicExample example) {
        return topicMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(TopicExample example) {
        return topicMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return topicMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Topic record) {
        return topicMapper.insert(record);
    }

    @Override
    public int insertSelective(Topic record) {
        return topicMapper.insertSelective(record);
    }

    @Override
    public List<Topic> selectByExample(TopicExample example) {
        return topicMapper.selectByExample(example);
    }

    @Override
    public Topic selectByPrimaryKey(Integer id) {
        return topicMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Topic record,TopicExample example) {
        return topicMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Topic record,TopicExample example) {
        return topicMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Topic record) {
        return topicMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Topic record) {
        return topicMapper.updateByPrimaryKey(record);
    }

}
