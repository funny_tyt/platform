package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.mapper.SubjectMapper;
import cn.jasonone.bean.SubjectExample;
import java.util.List;
import cn.jasonone.bean.Subject;
import cn.jasonone.service.SubjectService;
@Service
public class SubjectServiceImpl implements SubjectService{

    @Resource
    private SubjectMapper subjectMapper;

    @Override
    public long countByExample(SubjectExample example) {
        return subjectMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(SubjectExample example) {
        return subjectMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return subjectMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Subject record) {
        return subjectMapper.insert(record);
    }

    @Override
    public int insertSelective(Subject record) {
        return subjectMapper.insertSelective(record);
    }

    @Override
    public List<Subject> selectByExample(SubjectExample example) {
        return subjectMapper.selectByExample(example);
    }

    @Override
    public Subject selectByPrimaryKey(Integer id) {
        return subjectMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Subject record,SubjectExample example) {
        return subjectMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Subject record,SubjectExample example) {
        return subjectMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Subject record) {
        return subjectMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Subject record) {
        return subjectMapper.updateByPrimaryKey(record);
    }

}
