package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.mapper.GradeMapper;
import java.util.List;
import cn.jasonone.bean.Grade;
import cn.jasonone.bean.GradeExample;
import cn.jasonone.service.GradeService;
@Service
public class GradeServiceImpl implements GradeService{

    @Resource
    private GradeMapper gradeMapper;

    @Override
    public long countByExample(GradeExample example) {
        return gradeMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(GradeExample example) {
        return gradeMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return gradeMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Grade record) {
        return gradeMapper.insert(record);
    }

    @Override
    public int insertSelective(Grade record) {
        return gradeMapper.insertSelective(record);
    }

    @Override
    public List<Grade> selectByExample(GradeExample example) {
        return gradeMapper.selectByExample(example);
    }

    @Override
    public Grade selectByPrimaryKey(Integer id) {
        return gradeMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Grade record,GradeExample example) {
        return gradeMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Grade record,GradeExample example) {
        return gradeMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Grade record) {
        return gradeMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Grade record) {
        return gradeMapper.updateByPrimaryKey(record);
    }

}
