package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.bean.TestTopicMapping;
import cn.jasonone.bean.TestTopicMappingExample;
import java.util.List;
import cn.jasonone.mapper.TestTopicMappingMapper;
import cn.jasonone.service.TestTopicMappingService;
@Service
public class TestTopicMappingServiceImpl implements TestTopicMappingService{

    @Resource
    private TestTopicMappingMapper testTopicMappingMapper;

    @Override
    public long countByExample(TestTopicMappingExample example) {
        return testTopicMappingMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(TestTopicMappingExample example) {
        return testTopicMappingMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer testId,Integer topicId) {
        return testTopicMappingMapper.deleteByPrimaryKey(testId,topicId);
    }

    @Override
    public int insert(TestTopicMapping record) {
        return testTopicMappingMapper.insert(record);
    }

    @Override
    public int insertSelective(TestTopicMapping record) {
        return testTopicMappingMapper.insertSelective(record);
    }

    @Override
    public List<TestTopicMapping> selectByExample(TestTopicMappingExample example) {
        return testTopicMappingMapper.selectByExample(example);
    }

    @Override
    public int updateByExampleSelective(TestTopicMapping record,TestTopicMappingExample example) {
        return testTopicMappingMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(TestTopicMapping record,TestTopicMappingExample example) {
        return testTopicMappingMapper.updateByExample(record,example);
    }

}
