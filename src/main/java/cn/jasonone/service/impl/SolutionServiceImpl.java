package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import cn.jasonone.mapper.SolutionMapper;
import java.util.List;
import cn.jasonone.bean.SolutionExample;
import cn.jasonone.bean.Solution;
import cn.jasonone.service.SolutionService;
@Service
public class SolutionServiceImpl implements SolutionService{

    @Resource
    private SolutionMapper solutionMapper;

    @Override
    public long countByExample(SolutionExample example) {
        return solutionMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(SolutionExample example) {
        return solutionMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return solutionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Solution record) {
        return solutionMapper.insert(record);
    }

    @Override
    public int insertSelective(Solution record) {
        return solutionMapper.insertSelective(record);
    }

    @Override
    public List<Solution> selectByExample(SolutionExample example) {
        return solutionMapper.selectByExample(example);
    }

    @Override
    public Solution selectByPrimaryKey(Integer id) {
        return solutionMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(Solution record,SolutionExample example) {
        return solutionMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(Solution record,SolutionExample example) {
        return solutionMapper.updateByExample(record,example);
    }

    @Override
    public int updateByPrimaryKeySelective(Solution record) {
        return solutionMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Solution record) {
        return solutionMapper.updateByPrimaryKey(record);
    }

}
