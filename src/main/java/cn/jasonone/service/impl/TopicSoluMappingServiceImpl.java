package cn.jasonone.service.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.jasonone.mapper.TopicSoluMappingMapper;
import cn.jasonone.bean.TopicSoluMappingExample;
import cn.jasonone.bean.TopicSoluMapping;
import cn.jasonone.service.TopicSoluMappingService;
@Service
public class TopicSoluMappingServiceImpl implements TopicSoluMappingService{

    @Resource
    private TopicSoluMappingMapper topicSoluMappingMapper;

    @Override
    public long countByExample(TopicSoluMappingExample example) {
        return topicSoluMappingMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(TopicSoluMappingExample example) {
        return topicSoluMappingMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(Integer topicId,Integer soluId) {
        return topicSoluMappingMapper.deleteByPrimaryKey(topicId,soluId);
    }

    @Override
    public int insert(TopicSoluMapping record) {
        return topicSoluMappingMapper.insert(record);
    }

    @Override
    public int insertSelective(TopicSoluMapping record) {
        return topicSoluMappingMapper.insertSelective(record);
    }

    @Override
    public List<TopicSoluMapping> selectByExample(TopicSoluMappingExample example) {
        return topicSoluMappingMapper.selectByExample(example);
    }

    @Override
    public int updateByExampleSelective(TopicSoluMapping record,TopicSoluMappingExample example) {
        return topicSoluMappingMapper.updateByExampleSelective(record,example);
    }

    @Override
    public int updateByExample(TopicSoluMapping record,TopicSoluMappingExample example) {
        return topicSoluMappingMapper.updateByExample(record,example);
    }

}
