package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.Test;
import cn.jasonone.bean.TestExample;
public interface TestService{


    long countByExample(TestExample example);

    int deleteByExample(TestExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Test record);

    int insertSelective(Test record);

    List<Test> selectByExample(TestExample example);

    Test selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Test record,TestExample example);

    int updateByExample(Test record,TestExample example);

    int updateByPrimaryKeySelective(Test record);

    int updateByPrimaryKey(Test record);

}
