package cn.jasonone.service;

import java.util.List;
import cn.jasonone.bean.TopicSoluMappingExample;
import cn.jasonone.bean.TopicSoluMapping;
public interface TopicSoluMappingService{


    long countByExample(TopicSoluMappingExample example);

    int deleteByExample(TopicSoluMappingExample example);

    int deleteByPrimaryKey(Integer topicId,Integer soluId);

    int insert(TopicSoluMapping record);

    int insertSelective(TopicSoluMapping record);

    List<TopicSoluMapping> selectByExample(TopicSoluMappingExample example);

    int updateByExampleSelective(TopicSoluMapping record,TopicSoluMappingExample example);

    int updateByExample(TopicSoluMapping record,TopicSoluMappingExample example);

}
