package cn.jasonone.service;

import cn.jasonone.bean.TypeExample;
import java.util.List;
import cn.jasonone.bean.Type;
public interface TypeService{


    long countByExample(TypeExample example);

    int deleteByExample(TypeExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Type record);

    int insertSelective(Type record);

    List<Type> selectByExample(TypeExample example);

    Type selectByPrimaryKey(Integer id);

    int updateByExampleSelective(Type record,TypeExample example);

    int updateByExample(Type record,TypeExample example);

    int updateByPrimaryKeySelective(Type record);

    int updateByPrimaryKey(Type record);

}
