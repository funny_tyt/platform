package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Solution implements Serializable {
    /**
    * 主键、选项id（答案）
    */
    private Integer id;

    /**
    * 选项标签（ABCD或者填空题标准答案）
    */
    private String tag;

    /**
    * 选项描述（填空题描述为空）
    */
    private String text;

    /**
    * 外键、题目
    */
    private Integer topicId;

    private static final long serialVersionUID = 1L;
}