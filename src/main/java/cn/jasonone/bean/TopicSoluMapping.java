package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicSoluMapping implements Serializable {
    /**
    * 题目id主键
    */
    private Integer topicId;

    /**
    * 选项id主键
    */
    private Integer soluId;

    private static final long serialVersionUID = 1L;
}