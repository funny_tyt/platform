package cn.jasonone.bean;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Type implements Serializable {
    /**
    * 题目类型id
    */
    private Integer id;

    /**
    * 类型名称
    */
    private String name;

    private static final long serialVersionUID = 1L;
}