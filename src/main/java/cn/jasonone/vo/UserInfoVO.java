package cn.jasonone.vo;

import cn.jasonone.bean.UserInfo;
import lombok.Data;

@Data
public class UserInfoVO extends UserInfo {
	private int[] roleIds;
}
