package cn.jasonone.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpResult<T> {
	public static final HttpResult SUCCESS=new HttpResult(0);
	
	private int code;
	private String message;
	private long count;
	private List<T> data;
	
	public HttpResult(int code) {
		this.code = code;
	}
	
	public HttpResult(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public HttpResult(int code, String message, List<T> data) {
		this.code = code;
		this.message = message;
		this.data = data;
		this.count=data!=null?data.size():0L;
	}
}
