package cn.jasonone.mapper;

import cn.jasonone.bean.TopicSoluMapping;
import cn.jasonone.bean.TopicSoluMappingExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface TopicSoluMappingMapper {
    long countByExample(TopicSoluMappingExample example);

    int deleteByExample(TopicSoluMappingExample example);

    int deleteByPrimaryKey(@Param("topicId") Integer topicId, @Param("soluId") Integer soluId);

    int insert(TopicSoluMapping record);

    int insertSelective(TopicSoluMapping record);

    List<TopicSoluMapping> selectByExample(TopicSoluMappingExample example);

    int updateByExampleSelective(@Param("record") TopicSoluMapping record, @Param("example") TopicSoluMappingExample example);

    int updateByExample(@Param("record") TopicSoluMapping record, @Param("example") TopicSoluMappingExample example);
}